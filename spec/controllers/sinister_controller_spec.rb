require 'rails_helper'

RSpec.describe SinistersController, :type => :controller do

  let(:sinister) do
    Sinister.new({
      id: 1,
      title: 'title',
      description: 'description',
      sinister_date: DateTime.new(2014, 01, 02),
      longitude: '25',
      latitude: '24'
    })
  end

  let(:sinisters) do
    [
      Sinister.new({ description: 'sinister 1' }),
      Sinister.new({ description: 'sinister 2' })
    ]
  end

  context 'user is guest' do
    let :user do
      User.new({
        email: 'user@user.com',
        name: 'User Name'
      })
    end

    describe 'GET index' do

      before(:each) do
        allow(Sinister).to receive(:actives) { sinisters }
        allow(controller).to receive(:current_user) { user }
        allow(sinisters).to receive(:approved) { sinisters }
        allow(sinisters).to receive(:order) { sinisters }
        get 'index'
      end

      it 'returns sinisters list' do
        expect(assigns(:sinisters)).to be(sinisters)
      end

      it 'return http success' do
        expect(response).to be_success
      end
    end
  end

  context 'user is not admin' do
    let :user do
      User.new({
        email: 'user@user.com',
        name: 'User Name'
      })
    end

    before(:each) do
      allow(controller).to receive(:current_user) { user }
    end

    describe 'GET index' do
      before(:each) do
        allow(Sinister).to receive(:actives) { sinisters }
        allow(sinisters).to receive(:approved) { sinisters }
        allow(sinisters).to receive(:order) { sinisters }
        get 'index'
      end
    end

    describe 'GET sinisters/:id/edit' do
      before(:each) do
        allow(Sinister).to receive(:find) { sinister }
        get 'edit', id: sinister.id
      end
      it 'returns access denied' do
        expect(response).to be_forbidden
      end
    end
  end

  context 'user is admin' do 
    let :user do
      User.new({
        email: 'user@user.com',
        name: 'User Name',
        is_admin: true
      })
    end

    before(:each) do
      allow(controller).to receive(:current_user) { user }
    end
    
    describe 'GET sinisters/:id' do
      before(:each) do
        allow(Sinister).to receive(:find) { sinister }
        get "show", id: sinister.id
      end

      it 'returns http success' do
        expect(response).to be_success
      end

      it 'renders the correct sinister' do
        expect(assigns(:sinister)).to be(sinister)
      end
    end

    describe 'GET index' do
      let(:sinisters) do
        [
          Sinister.new({ description: 'sinister 1' }),
          Sinister.new({ description: 'sinister 2' })
        ]
      end

      context 'without search' do
        before(:each) do
          allow(Sinister).to receive(:actives) { sinisters }
          allow(sinisters).to receive(:order) { sinisters }
          get 'index'
        end

        it 'returns sinisters list' do
          expect(assigns(:sinisters)).to be(sinisters)
        end

        it 'returns http success' do
          expect(response).to be_success
        end
      end

      context 'with search' do
        before(:each) do
          expect(Sinister).to receive(:search) { sinisters }
          allow(sinisters).to receive(:actives) { sinisters }
          allow(sinisters).to receive(:order) { sinisters }
          get 'index', desc_or_title: '1'
        end

        it 'returns sinisters list' do
          expect(assigns(:sinisters)).to be(sinisters)
        end 
        
        it 'returns http success' do
          expect(response).to be_success
        end
      end
    end

    describe "GET 'new'" do
      it 'returns http success' do
        get 'new'
        expect(response).to be_success
      end

      it 'returns a new empty model' do
        get 'new'
        expect(assigns(:sinister)).to be_a(Sinister)
      end
    end

    describe 'POST sinisters' do
      context 'with correct data' do
        # We can not use a let block for sinister since the first call to sinister
        # is during the stubbing of Sinister.new. So Sinister.new would
        # be stub to return Sinister.new (we do that to have a spy) and it that
        # stub would be call to provide the result of that same stub. That would
        # result in an infinite recursive call. Note that there is probably a
        # better way to achieve that
        sinister = nil
        let(:sinister_params) do
          { title: 'title', 
            description: 'description', 
            sinister_date: DateTime.new(2014, 01, 02), 
            longitude: '25', 
            latitude: '26'
          }
        end

        let(:now) do
          DateTime.new(2014, 01, 01)
        end

        before(:each) do
          sinister = Sinister.new
          allow(DateTime).to receive(:now) { now }
          allow(Sinister).to receive(:new) { sinister }
          allow(sinister).to receive(:save) { true }
          post 'create', sinister: sinister_params
        end

        it 'should redirect_to index' do
          expect(response).to redirect_to action: :index
        end

        it 'should add sinister to database' do
          expect(sinister.title).to eq('title')
          expect(sinister.description).to eq('description')
          expect(sinister.sinister_date).to eq(DateTime.new(2014, 01, 02))
          expect(sinister.longitude).to eq(25)
          expect(sinister.latitude).to eq(26)
        end
      end
    end

    describe 'GET sinisters/:id/edit' do
      before(:each) do
        allow(Sinister).to receive(:find) { sinister }
        get 'edit', id: sinister.id
      end

      it 'returns http success' do
        expect(response).to be_success
      end

      it 'renders the correct sinister' do
        expect(assigns(:sinister)).to be(sinister)
      end
    end


    describe 'PATCH sinisters/:id' do

      before(:each) do
        allow(Sinister).to receive(:find) { sinister }
      end

      context 'with valid data' do
        let(:sinister_params) do
          { title: 'new title', 
            description: 'new description', 
            sinister_date: DateTime.new(2014, 02, 02), 
            longitude: '29', 
            latitude: '28'
          }
        end

        before(:each) do
          allow(sinister).to receive(:save) { true }
          patch 'update', id: sinister.id, sinister: sinister_params
        end

        it 'redirects to index' do
          expect(response).to redirect_to action: :index
        end

        it 'updates sinister' do
          expect(sinister.title).to eq('new title')
          expect(sinister.description).to eq('new description')
          expect(sinister.sinister_date).to eq(DateTime.new(2014, 02, 02))
          expect(sinister.longitude).to eq(29)
          expect(sinister.latitude).to eq(28)
        end
      end

      context 'with invalid date' do
        before(:each) do
          allow(sinister).to receive(:save) { false }
          patch 'update', id: sinister.id, sinister: { name: '', description: 'new description' }
        end

        it 'renders edit view' do
          expect(response).to be_success
          expect(response).to render_template(:edit)
        end

        it 'renders the correct sinister' do
          expect(sinister.title).to eq('title')
          expect(sinister.description).to eq('new description')
          expect(sinister.sinister_date).to eq(DateTime.new(2014, 01, 02))
          expect(sinister.longitude).to eq(25)
          expect(sinister.latitude).to eq(24)
        end
      end
    end

    describe 'DELETE sinisters/:id' do
      before(:each) do
        allow(Sinister).to receive(:find) { sinister }
        request.env['HTTP_REFERER'] = 'back'
      end 
      context 'delete with success' do
        before(:each) do
          expect(sinister).to receive(:soft_delete) { true }
          delete 'destroy', id: sinister.id
        end
        it 'redirect_to back' do
          expect(response).to redirect_to('back')
        end
      end
    end

    describe 'PATCH sinisters/:sinister_id/approve' do
      before(:each) do
        allow(Sinister).to receive(:find) { sinister }
        request.env['HTTP_REFERER'] = 'back'
      end 
      context 'update with success' do
        before(:each) do
          expect(sinister).to receive(:approve) { true }
          patch 'approve', sinister_id: sinister.id
        end
        it 'redirect_to back' do
          expect(response).to redirect_to('back')
        end
      end
    end

    describe 'PATCH sinisters/:sinister_id/unapprove' do
      before(:each) do
        allow(Sinister).to receive(:find) { sinister }
        request.env['HTTP_REFERER'] = 'back'
      end 
      context 'update with success' do
        before(:each) do
          expect(sinister).to receive(:unapprove) { true }
          patch 'unapprove', sinister_id: sinister.id
        end
        it 'redirect_to back' do
          expect(response).to redirect_to('back')
        end
      end
    end
  end
end