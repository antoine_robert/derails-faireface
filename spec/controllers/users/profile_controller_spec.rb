require 'rails_helper'

describe Users::ProfileController, :type => :controller do
  let :user do
    User.new({
      email: 'user@user.com',
      name: 'User Name'
    })
  end

  before(:each) do
    allow(controller).to receive(:current_user) { user }
  end

  describe 'GET profile' do
    it 'returns http success' do
      get 'edit'
      expect(response).to be_success
      # We check if the controller correctly assign @user
      expect(assigns(:user)).to be(user)
    end
  end

  describe 'PATCH profile' do
    context 'with valid data' do
      before(:each) do
        allow(user).to receive(:save) { true }
        patch 'update', user: { name: "New User" }
      end

      it 'should return http success' do
        expect(response).to redirect_to(action: :edit)
      end

      it 'should update user name' do
        expect(user.name).to eq('New User')
      end
    end

    context 'with invalid data' do
      before(:each) do
        allow(user).to receive(:save) { false }
        patch 'update', user: { name: "" }
      end

      it 'should return view' do
        expect(response).to be_success
      end
    end
  end

  describe 'DELETE profile' do
    context 'with valid data' do
      before(:each) do
        expect(controller).to receive(:sign_out)
        expect(user).to receive(:soft_delete) { true }
        delete 'destroy'
      end

      it 'should update active status' do
      end

      it 'should sign out the user' do
      end

      it 'should redirect to index' do
        expect(response).to redirect_to(root_path)
      end

    end 

  end
  
end
