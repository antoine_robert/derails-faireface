# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150427023311) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "alerts", force: true do |t|
    t.string   "title"
    t.integer  "sinister_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "publication_date"
    t.string   "message"
    t.datetime "published_at"
  end

  add_index "alerts", ["sinister_id"], name: "index_alerts_on_sinister_id", using: :btree

  create_table "categories", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "emergencies", force: true do |t|
    t.string   "organization"
    t.string   "phone"
    t.string   "web_site"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "information_bulletins", force: true do |t|
    t.string   "title"
    t.text     "body"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.date     "deleted_at"
  end

  add_index "information_bulletins", ["user_id"], name: "index_information_bulletins_on_user_id", using: :btree

  create_table "news", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.integer  "user_id"
    t.datetime "created_at"
    t.date     "deleted_at"
    t.datetime "updated_at"
  end

  add_index "news", ["user_id"], name: "index_news_on_user_id", using: :btree

  create_table "sinisters", force: true do |t|
    t.string   "title",              null: false
    t.text     "description",        null: false
    t.datetime "sinister_date",      null: false
    t.decimal  "longitude",          null: false
    t.decimal  "latitude",           null: false
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.datetime "approved_at"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.integer  "category_id",        null: false
  end

  add_index "sinisters", ["category_id"], name: "index_sinisters_on_category_id", using: :btree
  add_index "sinisters", ["user_id"], name: "index_sinisters_on_user_id", using: :btree

  create_table "students", force: true do |t|
    t.string   "name"
    t.date     "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                    default: "",    null: false
    t.string   "encrypted_password",       default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",            default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "provider"
    t.string   "uid"
    t.string   "name"
    t.boolean  "is_admin",                 default: false
    t.string   "phone_number"
    t.boolean  "is_subscribe_mail_alert"
    t.boolean  "is_subscribe_phone_alert"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
