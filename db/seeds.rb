#Utilisateur

users = [{
  :email => 'user@user.com',  
  :sign_in_count => 1, 
  :current_sign_in_at => Time.zone.now, 
  :last_sign_in_at => Time.zone.now,
  :confirmed_at => Time.zone.now, 
  :confirmation_sent_at => Time.zone.now, 
  :created_at => Time.zone.now, 
  :updated_at => Time.zone.now,
  :name => 'user123', 
  :password => 'qwerty12345', 
  :is_admin => false },{
  :email => 'admin@admin.com',  
  :sign_in_count => 1, 
  :current_sign_in_at => Time.zone.now, 
  :last_sign_in_at => Time.zone.now,
  :confirmed_at => Time.zone.now, 
  :confirmation_sent_at => Time.zone.now, 
  :created_at => Time.zone.now, 
  :updated_at => Time.zone.now,
  :name => 'admin123', 
  :password => 'qwerty12345', 
  :is_admin => true }]

users.map {|user| User.new(user).save } 

#Categories

categories = [{
  :name => 'Chute de météorites',
  :created_at => Time.zone.now,
  :updated_at => Time.zone.now},{
  :name => 'Incendie',
  :created_at => Time.zone.now,
  :updated_at => Time.zone.now},{
  :name => 'Canicule',
  :created_at => Time.zone.now,
  :updated_at => Time.zone.now},{
  :name => 'Feu de forêt',
  :created_at => Time.zone.now,
  :updated_at => Time.zone.now},{
  :name => 'Glissement de terrain',
  :created_at => Time.zone.now,
  :updated_at => Time.zone.now}]

categories.map {|category| Category.new(category).save } 

#Sinistre1
s1 = Sinister.create(
  category: Category.find_by(name: 'Chute de météorites'),
  title: 'Chute de météorites sur le château Frontenac',
  description: 'Des centaines de météorites ont bombardé le château Frontenac. Un seul survivant', 
  sinister_date:Time.zone.now, 
  approved_at: Time.zone.now, 
  longitude: '46.790876', 
  latitude: '-71.2621108')
s1.image = File.open(Rails.root + "app/assets/images/seedimage1.jpg")
s1.save!

#Sinistre2
s2 = Sinister.create(
  category: Category.find_by(name: 'Canicule'),
  user: User.take,
  title: 'Canicule à Charlesbourg',
  description: 'Une canicule vient de frapper la rue de la Faune à Charlesbourg', 
  sinister_date:Time.zone.now, 
  approved_at: Time.zone.now, 
  longitude: '46.790876', 
  latitude: '-71.2621108')
s2.image = File.open(Rails.root + "app/assets/images/seedimage2.jpg")
s2.save!

#Sinistre3
s3 = Sinister.create(
  category: Category.find_by(name: 'Feu de forêt'),
  title: 'Feu de forêt à Cap-Rouge',
  description: 'Feu de forêt dans la ville de Cap-Rouge produit beaucoup de fumées', 
  sinister_date:Time.zone.now, 
  approved_at: Time.zone.now, 
  longitude: '46.790876', 
  latitude: '-71.2621108')
s3.image = File.open(Rails.root + "app/assets/images/seedimage3.jpg")
s3.save!

#Sinistre4
s4 = Sinister.create(
  category: Category.find_by(name: 'Glissement de terrain'),
  title: 'Glissement de terrain dans le Vieux-Québec',
  description: 'Glissement de terrain causé par les pluies et le dégel printanier dans le Vieux-Québec', 
  sinister_date:Time.zone.now, 
  approved_at: Time.zone.now, 
  longitude: '46.790876', 
  latitude: '-71.2621108')
s4.image = File.open(Rails.root + "app/assets/images/seedimage4.jpg")
s4.save!

################# News #################
new1 = New.create(
  title: 'Lorem Ipsum',
  user: User.take,
  description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In quis pharetra diam. Etiam scelerisque.'
  )
new1.save!

new2 = New.create(
  title: 'Lorem Ipsum 2',
  user: User.take,
  description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ornare lacus a molestie volutpat. Praesent finibus semper.'
  )
new2.save!

new3 = New.create(
  title: 'Lorem Ipsum 3',
  user: User.take,
  description: 'Enim sed tristique. Nam luctus non metus in commodo. Vestibulum semper purus enim, ac suscipit nunc posuere at.'
  )
new3.save!

new4 = New.create(
  title: 'Lorem Ipsum 4',
  user: User.take,
  description: 'Vestibulum et lectus metus. Nullam nec auctor sem. Mauris porttitor hendrerit iaculis. Nulla et nibh non leo imperdiet luctus.'
  )
new4.save!

new5 = New.create(
  title: 'Lorem Ipsum 5',
  user: User.take,
  description: 'Sed molestie eros nec felis varius, ac ornare metus pretium. In euismod velit id pretium consequat. Sed aliquam elit at viverra.'
  )
new5.save!

new6 = New.create(
  title: 'Lorem Ipsum 6',
  user: User.take,
  description: 'Sed pharetra nisi a dignissim commodo. Donec interdum tellus sed purus sollicitudin, nec venenatis lorem efficitur. Praesent aios.'
  )
new6.save!

#InformationBulletin1
ib1 = InformationBulletin.create(
  title: 'Préparation en cas de glissement de terrain',
  body: 'Attachez bien vos souliers et évacuez les lieux!')
ib1.image = File.open(Rails.root + "app/assets/images/seedimage4.jpg")
ib1.save!

#InformationBulletin2
ib2 = InformationBulletin.create(
  title: 'Préparation en cas de de canicule',
  body: "Buvez beaucoup d'eau et trouvez un endroit frais!")
ib2.image = File.open(Rails.root + "app/assets/images/seedimage2.jpg")
ib2.save!

#InformationBulletin3
ib3 = InformationBulletin.create(
  title: 'Préparation en cas de feu de forêt à Cap-Rouge',
  body: "Tenez-vous loin de l'arrondissement de Cap-Rouge!")
ib3.image = File.open(Rails.root + "app/assets/images/seedimage3.jpg")
ib3.save!

#InformationBulletin4
ib4 = InformationBulletin.create(
  title: "Comment vous préparez en cas d'inondation!",
  body: 'Évacuez les lieux et trouver un endroit non inondé où rester!')
ib4.image = File.open(Rails.root + "app/assets/images/seedimage5.jpg")
ib4.save!
