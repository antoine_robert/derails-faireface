class CreateSinisters < ActiveRecord::Migration
  def change
    create_table :sinisters do |t|
      t.string :titre
      t.text :description
      t.datetime :sinister_date
      t.datetime :submission_date
      t.decimal :longitude
      t.decimal :latitude
      t.datetime :deleted_at

      t.timestamps
    end
  end
end
