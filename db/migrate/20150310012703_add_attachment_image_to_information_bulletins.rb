class AddAttachmentImageToInformationBulletins < ActiveRecord::Migration
  def self.up
    change_table :information_bulletins do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :information_bulletins, :image
  end
end
