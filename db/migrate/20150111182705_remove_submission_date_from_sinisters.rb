class RemoveSubmissionDateFromSinisters < ActiveRecord::Migration
  def change
    remove_column :sinisters, :submission_date, :datetime
  end
end
