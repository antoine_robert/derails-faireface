class CreateEmergencies < ActiveRecord::Migration
  def change
    create_table :emergencies do |t|
      t.string :organization
      t.string :phone
      t.string :web_site

      t.timestamps
    end
  end
end
