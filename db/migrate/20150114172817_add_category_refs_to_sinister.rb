class AddCategoryRefsToSinister < ActiveRecord::Migration
  def change
    add_reference :sinisters, :category, index: true
  end
end
