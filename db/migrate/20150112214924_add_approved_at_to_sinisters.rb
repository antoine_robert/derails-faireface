class AddApprovedAtToSinisters < ActiveRecord::Migration
  def change
    add_column :sinisters, :approved_at, :datetime
  end
end
