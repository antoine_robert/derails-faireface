class AddDeletedAtToInformationBulletins < ActiveRecord::Migration
  def change
    add_column :information_bulletins, :deleted_at, :date
  end
end
