class AddUserRefsToSinister < ActiveRecord::Migration
  def change
    add_reference :sinisters, :user, index: true
  end
end
