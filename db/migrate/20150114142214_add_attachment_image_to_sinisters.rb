class AddAttachmentImageToSinisters < ActiveRecord::Migration
  def self.up
    change_table :sinisters do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :sinisters, :image
  end
end
