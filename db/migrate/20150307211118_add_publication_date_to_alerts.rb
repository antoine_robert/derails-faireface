class AddPublicationDateToAlerts < ActiveRecord::Migration
  def change
    add_column :alerts, :publication_date, :timestamp
  end
end
