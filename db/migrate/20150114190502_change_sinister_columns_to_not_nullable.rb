class ChangeSinisterColumnsToNotNullable < ActiveRecord::Migration
  change_table :sinisters do |t|
    t.change :title, :string, null: false
    t.change :description, :text, null: false
    t.change :sinister_date, :datetime, null: false
    t.change :longitude, :decimal, null: false
    t.change :latitude, :decimal, null: false
    t.change :category_id, :integer, null: false
  end
end
