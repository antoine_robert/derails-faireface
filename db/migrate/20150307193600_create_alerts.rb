class CreateAlerts < ActiveRecord::Migration
  def change
    create_table :alerts do |t|
      t.string :title
      t.belongs_to :sinister, index: true

      t.timestamps
    end
  end
end
