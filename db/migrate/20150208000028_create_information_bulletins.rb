class CreateInformationBulletins < ActiveRecord::Migration
  def change
    create_table :information_bulletins do |t|
      t.string :title
      t.text :body
      t.references :user, index: true

      t.timestamps
    end
  end
end
