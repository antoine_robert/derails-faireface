class AddSubscribtionOptionToUsers < ActiveRecord::Migration
  def change
    add_column :users, :is_subscribe_mail_alert, :boolean
    add_column :users, :is_subscribe_phone_alert, :boolean
  end
end
