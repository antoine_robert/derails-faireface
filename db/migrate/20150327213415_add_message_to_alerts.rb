class AddMessageToAlerts < ActiveRecord::Migration
  def change
    add_column :alerts, :message, :string
  end
end
