class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.string :name
      t.date :deleted_at

      t.timestamps
    end
  end
end
