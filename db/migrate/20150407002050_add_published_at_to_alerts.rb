class AddPublishedAtToAlerts < ActiveRecord::Migration
  def change
    add_column :alerts, :published_at, :timestamp
  end
end
