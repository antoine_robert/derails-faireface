class CreateNews < ActiveRecord::Migration
  def change
    create_table :news do |t|
      t.string :title
      t.text :description
      t.references :user, index: true
      t.date :created_at
      t.date :deleted_at

      t.timestamps
    end
  end
end
