class RenameTitreColumnName < ActiveRecord::Migration
  def change
    rename_column :sinisters, :titre, :title
  end
end
