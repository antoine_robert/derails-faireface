Rails.application.routes.draw do  

  root to: 'home#index'
  devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks' }


  get '/profile', to: 'users/profile#edit'
  patch '/profile', to: 'users/profile#update'
  delete '/profile', to: 'users/profile#destroy'

  get '/emergencyplan', to: 'staticpages#emergencyplan'
  post '/emergencyplan', to: 'staticpages#generate_pdf'

  get "/staticpages/download_pdf"
  
  get 'cron/notifications', to: 'cron#notifications'

  resources :information_bulletins
  resources :emergencies

  resources :sinisters do
    patch :approve
    patch :unapprove

    resources :alerts, only: [:show, :new, :create]
  end

  resources :news
end
