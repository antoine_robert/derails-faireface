Devise.setup do |config|
  # The secret key used by Devise. Devise uses this key to generate
  # random tokens. Changing this key will render invalid all existing
  # confirmation, reset password and unlock tokens in the database.
   config.secret_key = 'e2da2b0ad4c332529b9592f0e7362707467b469723733bc9f3cab85bdac1cd1fd853f67a348dcb5f7416afe65013df69fe23251ea7a2405784ea79dcb6ab4a50'

  # ==> Mailer Configuration
  # Configure the e-mail address which will be shown in Devise::Mailer,
  # note that it will be overwritten if you use your own mailer class
  # with default "from" parameter.
   config.mailer_sender = 'yanickouellet555@gmail.com'

  # Configure the class responsible to send e-mails.
  config.mailer = 'Devise::Mailer'

  # ==> ORM configuration
  require 'devise/orm/active_record'

  # ==> Configuration for any authentication mechanism
   config.authentication_keys = [ :email ]

  config.case_insensitive_keys = [ :email ]

  config.strip_whitespace_keys = [ :email ]

  config.skip_session_storage = [:http_auth]

  config.stretches = Rails.env.test? ? 1 : 10

  # Setup a pepper to generate the encrypted password.
   config.pepper = 'f7c72daca75563ac14134b541bc1ff117090b3b356ab3635f6aebd9d3f23176874b0805de8b40d8ff71f954c7442eceae457f5a4910c6ec17c69f9086e12e312'

  # ==> Configuration for :confirmable
  config.allow_unconfirmed_access_for = 0.days

  config.reconfirmable = true

  # ==> Configuration for :rememberable
  config.remember_for = 2.weeks

  # Invalidates all the remember me tokens when the user signs out.
  config.expire_all_remember_me_on_sign_out = true

  # ==> Configuration for :validatable
  # Range for password length.
  config.password_length = 6..128

  config.email_regexp = /\A[^@]+@[^@]+\z/

  # ==> Configuration for :timeoutable
  config.timeout_in = 45.minutes

  # ==> Configuration for :lockable
  config.lock_strategy = :none

  # ==> Configuration for :recoverable
  #
  # Defines which key will be used when recovering the password for an account
  config.reset_password_keys = [ :email ]

  config.reset_password_within = 6.hours

  # The default HTTP method used to sign out a resource. Default is :delete.
  config.sign_out_via = :delete

  # ==> OmniAuth
  config.omniauth :facebook, ENV['FACEBOOK_APPID'], ENV['FACEBOOK_APP_SECRET'], scope: ['email']
  config.omniauth :google_oauth2, ENV['GOOGLE_APPID'], ENV['GOOGLE_APP_SECRET']

  # ==> Warden configuration
  # If you want to use other strategies, that are not supported by Devise, or
  # change the failure app, you can configure them inside the config.warden block.
  #
  # config.warden do |manager|
  #   manager.intercept_401 = false
  #   manager.default_strategies(scope: :user).unshift :some_external_strategy
  # end

  # ==> Mountable engine configurations
  # When using Devise inside an engine, let's call it `MyEngine`, and this engine
  # is mountable, there are some extra configurations to be taken into account.
  # The following options are available, assuming the engine is mounted as:
  #
  #     mount MyEngine, at: '/my_engine'
  #
  # The router that invoked `devise_for`, in the example above, would be:
  # config.router_name = :my_engine
  #
  # When using omniauth, Devise cannot automatically set Omniauth path,
  # so you need to do it manually. For the users scope, it would be:
  # config.omniauth_path_prefix = '/my_engine/users/auth'
end
