class Alert < ActiveRecord::Base
  belongs_to :sinister
  validates :title, presence: { message: :title_required }
  validates :message, presence: { message: :message_required }
end
