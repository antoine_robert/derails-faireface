module Approvable
  extend ActiveSupport::Concern

  included do
    scope :approved, -> {where.not(approved_at: nil)}
    scope :unapproved, -> {where(approved_at: nil)}
  end

  def approved? 
    !approved_at.nil?
  end

  def approve
    update_attribute(:approved_at, DateTime.now)
  end

  def unapprove
    update_attribute(:approved_at, nil)
  end
end