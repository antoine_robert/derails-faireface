module OauthConnectReady
  extend ActiveSupport::Concern

  module ClassMethods
    def find_for_google_oauth(auth, signed_in_resource=nil)
      generic_find(auth)
    end

    def find_for_facebook_oauth(auth, signed_in_resource=nil)
      generic_find(auth)
    end

    private
      def generic_find(auth)
        where(provider: auth.provider, uid: auth.uid).first_or_create({
          email: auth.info.email.presence || '',
          provider: auth.provider,
          uid: auth.uid,
          name: auth.info.name,
          password: Devise.friendly_token[0,20],
          confirmed_at: DateTime.now
        })
      end
  end
end
