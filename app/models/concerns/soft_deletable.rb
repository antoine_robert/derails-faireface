module SoftDeletable
  extend ActiveSupport::Concern

  included do
    scope :actives, -> {where(deleted_at: nil)}
    scope :inactives, -> {where.not(deleted_at: nil)}
  end

  def deleted? 
    !deleted_at.nil?
  end

  def soft_delete
    update_attribute(:deleted_at, DateTime.now)
  end

end
