class InformationBulletin < ActiveRecord::Base
  include ::SoftDeletable
  belongs_to :user
  validates :title, presence: { message: :title_required }
  validates :body, presence: { message: :description_required }

  has_attached_file :image, :styles => { :medium => "300x300>", :small => "150x150>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/

  def self.search(query)
    where("lower(title) like ? or lower(body) like ?", "%#{query}%".downcase, "%#{query}%".downcase)
  end

end
