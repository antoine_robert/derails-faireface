class New < ActiveRecord::Base
  include ::SoftDeletable
  belongs_to :user
  before_create :set_default_date_to_now
  validates :title, presence: { message: :title_required }
  validates :description, presence: { message: :description_required }

  @is_cropped = false

  def set_default_date_to_now
  	self.created_at = Time.now
  end

  def cropped_description
    nb_words_max = 350
    if description.length > nb_words_max
      @is_cropped = true
      description.truncate(nb_words_max, :separator => ' ', omission: '')
    else
      description
    end
  end

  def is_cropped
    @is_cropped
  end
end
