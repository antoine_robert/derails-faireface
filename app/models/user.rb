class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable,
	 :recoverable, :rememberable, :trackable, :validatable,
	 :confirmable, :timeoutable,
   :omniauthable, :omniauth_providers => [:facebook, :google_oauth2]
  has_many :sinisters

  include ::SoftDeletable
  include ::OauthConnectReady

  def admin
    @user.is_admin == true
  end

  def active_for_authentication?
    super && !deleted_at?
  end

end
