class Sinister < ActiveRecord::Base
  include ::SoftDeletable, ::Approvable
  has_many :alerts
  belongs_to :user
  belongs_to :category
  validates :title, presence: { message: :title_required }
  validates :description, presence: { message: :description_required }
  validates :sinister_date, presence: true
  validates :longitude, presence: { message: :longitude_required }
  validates :latitude, presence: { message: :latitude_required }
  validates :category_id, presence: { message: :category_required }

  has_attached_file :image, :styles => { :medium => "300x300>", :small => "150x150>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/

  def self.search(query)
    where("lower(title) like ? or lower(description) like ?", "%#{query}%".downcase, "%#{query}%".downcase)
  end
end
