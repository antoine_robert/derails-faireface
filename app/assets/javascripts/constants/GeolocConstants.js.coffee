keyMirror = require 'keymirror'

module.exports = keyMirror({
  GEOLOC_UPDATE_ADDRESS: null,
  GEOLOC_UPDATE_LOCATION: null,
  GEOLOC_UPDATE_LOCATION_SUCCESS: null,
  GEOLOC_UPDATE_LOCATION_ERROR: null,
})
