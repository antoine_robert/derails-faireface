Plan = require '../components/plan.js.jsx.coffee'

if typeof(google) != "undefined"
  Geolocation = require '../components/geolocation.js.jsx.coffee'
  GeolocActions = require '../actions/GeolocActions'
  GeolocProvider = require '../providers/GeolocProvider'
  GeolocProvider.findLocation()

  $(document).on 'ready page:load', ->
    $('.geolocation-component').each (i, elem) ->
      React.render(
        `<Geolocation />`,
        elem
      )

$(document).on 'ready page:load', ->
  $('.emergency-plan').each (i, elem) ->
    React.render(
      `<Plan />`,
      elem
    )

