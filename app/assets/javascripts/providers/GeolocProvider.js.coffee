module.exports = provider = {}

GeolocStore = require ('../stores/GeolocStore')
EventEmitter = require('events').EventEmitter
GeolocActions = require '../actions/GeolocActions'
assign = require 'object-assign'
places = new google.maps.places.PlacesService(document.getElementById('gmap'))

_location = null

assign provider, {
  findLocation: ->
    GeolocActions.updateLocation()
    GeolocActions
    if `'geolocation' in navigator`
      navigator.geolocation.getCurrentPosition (location) ->
        GeolocActions.updateLocationSuccess(location)
    else
      GeolocActions.updateLocationFail()

  findAddressFromLocation: (location) ->
    return unless location
    GeolocActions.findAddress()
    $.get 'https://maps.googleapis.com/maps/api/geocode/json', {latlng: location.coords.latitude + ',' + location.coords.longitude}, (data) ->
      GeolocActions.findAddressSuccess(data.results[0].formatted_address)

  queryAddress: (address) ->
    GeolocActions.queryAddress()

    unless address
      GeolocActions.queryAddressEmpty()
      return

    places.textSearch {
      query: address
    }, (results, status) ->
      if status == google.maps.places.PlacesServiceStatus.OK
        GeolocActions.queryAddressSuccess(results)
      else
        GeolocActions.queryAddressError()

}

module.exports = provider
