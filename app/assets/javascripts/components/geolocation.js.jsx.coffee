GeolocStore = require '../stores/GeolocStore'
GeolocProvider = require '../providers/GeolocProvider'
GeolocActions = require '../actions/GeolocActions'

Autocomplete = require '../components/Autocomplete.js.jsx.coffee'

getGeolocState = ->
  {
    location: GeolocStore.getLocation(),
    address: GeolocStore.getAddress(),
    isAddressLocked: GeolocStore.isAddressLocked(),
    isProcessing: GeolocStore.isProcessing(),
    predictions: GeolocStore.getPredictions(),
    isUsingGeolocation: GeolocStore.isUsingGeolocation()
  }

Geolocation = React.createClass({
  getInitialState: ->
    getGeolocState()

  componentDidMount: ->
    GeolocStore.addChangeListener(this._onChange)

  componentWillUnmount: ->
    GeolocStore.removeChangeListener(this._onChange)

  _onChange: ->
    this.setState(getGeolocState())

  _onInput: (input) ->
    GeolocActions.updateAddress(input)

  _onUseAddress: (e) ->
    e.preventDefault()
    GeolocActions.toggleShowAddressInput()

  _onAddressSelected: (id) ->
    GeolocActions.selectAddress(id)

  render: ->
    latitude =  if this.state.location then this.state.location.latitude else ''
    longitude =  if this.state.location then this.state.location.longitude else ''
    loading = ''
    useAddress = ''
    addressInput = ''
    useAddressText = if this.state.isUsingGeolocation then "Entrer une adresse" else "Utiliser la géolocalisation"
    locationText = if this.state.isAddressLocked then this.state.address else ''
    
    if this.state.isProcessing 
      loading = `<span className="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>`
    else
      useAddress = `<a href="#" onClick={this._onUseAddress}>{useAddressText}</a>`

    unless this.state.isUsingGeolocation
      addressInput = `
        <div>
          <Autocomplete items={this.state.predictions} value={this.state.address} onInput={this._onInput} onItemSelected={this._onAddressSelected} />
        </div>
        `

    autocompleteValue = this.state.address

    `<div>
      <input type="hidden" name="sinister[longitude]" value={latitude} readOnly="true" />
      <input type="hidden" name="sinister[latitude]" value={longitude} readOnly="true" />
      <div>
        Location: {locationText}
      </div>
      {addressInput}
      {loading}
      {useAddress}
    </div>`
})

module.exports = Geolocation
