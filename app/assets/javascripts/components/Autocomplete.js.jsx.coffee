Item = React.createClass({
  propTypes: {
    value: React.PropTypes.string.isRequired
    onClick: React.PropTypes.func
    id: React.PropTypes.string
  }

  _onClick: ->
    this.props.onClick(this) if this.props.onClick

  render: ->
    component = `
      <div className="autocomplete-item" onClick={this._onClick}>
        {this.props.value}
      </div>
    `
    component
})


Autocomplete = React.createClass({
  propTypes: {
    items: React.PropTypes.array.isRequired
    onInput: React.PropTypes.func.isRequired
    onItemSelected: React.PropTypes.func
  }

  getInitialState: ->
    { dropdownActive: false }

  _onChange: (event) ->
    this._triggerInputEvent(event.target.value)
    this.setState({dropdownActive: true})

  _onItemClick: (item) ->
    this.props.onItemSelected(item.props.id) if this.props.onItemSelected
    this.setState({dropdownActive: false})

  _triggerInputEvent: (value) ->
    this.props.onInput(value) if this.props.onInput


  render: ->
    cx = React.addons.classSet
    itemsClasses = cx({
      'autocomplete-items': true,
      'autocomplete-dropdown-active': this.state.dropdownActive
    })

    items = this.props.items.map ((item) ->
      (`<Item key={item.id} id={item.id} value={item.value} onClick={this._onItemClick} />`)
    ).bind(this)

    component = `
      <div className="autocomplete">
        <input type="text" value={this.props.value} onChange={this._onChange} />
        <div className={itemsClasses}>
          {items}
        </div>
      </div>
    `
    component
})

module.exports = Autocomplete
