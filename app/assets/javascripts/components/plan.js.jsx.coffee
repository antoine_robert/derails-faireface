Field = React.createClass({
  render: ->
    o = `
      <div>
        <label htmlFor={this.props.id}>{this.props.label}</label>
        <input type="text" id={this.props.id} name={this.props.name} />
      </div>
    `
    o
})

Member = React.createClass({
  render: ->
    i = this.props.i
    component = `
    <div>
      <h3>Membre {i}</h3>
      <Field id={"name"+i} name={"m[][name]"} label="Nom" />
      <Field id={"birth"+i} name={"m[][birth]"} label="Date de naissance" />
      <Field id={"phone"+i} name={"m[][phone]"} label="Téléphone" />
      <Field id={"cell"+i} name={"m[][cell]"} label="Cellulaire" />
      <Field id={"email"+i} name={"m[][email]"} label="Courriel" />
      <Field id={"info"+i} name={"m[][info]"} label="Information médicale" />
      <Field id={"place"+i} name={"m[][place]"} label="Lieu fréquenté" />
      <Field id={"address"+i} name={"m[][address]"} label="Adresse" />
    </div>
    `
    component
})

Resource = React.createClass({
  render: ->
    i = this.props.i
    component = `
    <div>
      <h3>Personne {i}</h3>
      <Field id={"resname"+i} name={"r[][name]"} label="Nom" />
      <Field id={"resphone"+i} name={"r[][phone]"} label="Téléphone" />
      <Field id={"rescell"+i} name={"r[][cell]"} label="Cellulaire" />
      <Field id={"resemail"+i} name={"r[][email]"} label="Courriel" />
    </div>
    `
    component
})

Plan = React.createClass({
  getInitialState: ->
    memberCount: 1
    resourceCount: 1

  onAddMember: (e) ->
    e.preventDefault()
    @setState {memberCount: @state.memberCount + 1}

  onAddResource: (e) ->
    e.preventDefault()
    @setState {resourceCount: @state.resourceCount+ 1}

  render: ->
    members = []
    for i in [1 .. this.state.memberCount]
      members.push `<Member key={i} i={i} />`

    resources = []
    for i in [1 .. this.state.resourceCount]
      resources.push `<Resource key={i} i={i} />`

    component = `
      <form method="POST">
        <h2>Membres de votre famille</h2>
        {members}
        <a href="#" onClick={this.onAddMember}>Ajouter</a>

        <h2>Personnes ressources</h2>
        {resources}
        <a href="#" onClick={this.onAddResource}>Ajouter</a>
        <h2>Votre maison</h2>
        <Field id={"extincteur"} name={"extincteur"} label="Extincteur" />
        <Field id={"robinet"} name={"robinet"} label="Robinet d'entrée d'eau" />
        <Field id={"disjonc"} name={"disjonc"} label="Boîte de disjoncteurs" />
        <Field id={"gaz"} name={"gaz"} label="Robinet de gaz" />
        <Field id={"drain"} name={"drain"} label="Drain de sol" />

        <h2>Numéros importants</h2>
        <Field id={"arrond"} name={"arrond"} label="Arrondissement" />
        <Field id={"clin"} name={"clin"} label="Clinique médicale" />
        <Field id={"phar"} name={"phar"} label="Pharmacie" />
        <Field id={"vet"} name={"vet"} label="Vétérinaire" />
        <h3>Assurance habitation</h3>
        <Field id={"assur"} name={"assur"} label="Nom" />
        <Field id={"nopolice"} name={"nopolice"} label="Numéro de police" />
        <Field id={"assurtel"} name={"assurtel"} label="Téléphone" />

        <h2>Lieux de rassemblement</h2>
        <h3>Dans le quartier</h3>
        <Field id={"lqnom"} name={"lqnom"} label="Nom" />
        <Field id={"lqphone"} name={"lqphone"} label="Téléphone" />
        <h3>Hors du quartier</h3>
        <Field id={"lhnom"} name={"lhnom"} label="Nom" />
        <Field id={"lhphone"} name={"lhphone"} label="Téléphone" />

        <button type="submit">Générer le plan</button>
      </form>
    `
})

module.exports = Plan 
