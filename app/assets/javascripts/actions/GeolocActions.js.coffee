AppDispatcher = require '../dispatcher/AppDispatcher'
GeolocConstants = require '../constants/GeolocConstants'
GeolocProvider = require '../providers/GeolocProvider'

GeolocActions = {
  findAddress: (address) ->
    AppDispatcher.dispatch({
      actionType: 'GEOLOC_FIND_ADDRESS'
    })

  findAddressSuccess: (address) ->
    AppDispatcher.dispatch({
      actionType: 'GEOLOC_FIND_ADDRESS_SUCCESS'
      address: address
    })

  findAddressError: ->
    AppDispatcher.dispatch({
      actionType: 'GEOLOC_FIND_ADDRESS_ERROR'
    })

  updateLocation: ->
    AppDispatcher.dispatch({
      actionType: 'GEOLOC_UPDATE_LOCATION',
    })

  updateLocationSuccess: (location) ->
    AppDispatcher.dispatch({
      actionType: 'GEOLOC_UPDATE_LOCATION_SUCCESS',
      location: location
    })
    GeolocProvider.findAddressFromLocation(location)

  updateLocationFail: ->
    AppDispatcher.dispatch({actionType: 'GEOLOC_UPDATE_LOCATION_ERROR'})

  updateAddress: (address) ->
    AppDispatcher.dispatch({
      actionType: 'GEOLOC_UPDATE_ADDRESS',
      address: address
    })
    GeolocProvider.queryAddress(address)

  queryAddress: ->
    AppDispatcher.dispatch({actionType: 'GEOLOC_QUERY_ADDRESS'})

  queryAddressSuccess: (predictions) ->
    AppDispatcher.dispatch({
      actionType: 'GEOLOC_QUERY_ADDRESS_SUCCESS'
      predictions: predictions
    })

  queryAddressError: ->
    AppDispatcher.dispatch({actionType: 'GEOLOC_QUERY_ADDRESS_ERROR'})

  queryAddressEmpty: ->
    AppDispatcher.dispatch({actionType: 'GEOLOC_QUERY_ADDRESS_EMPTY'})

  toggleShowAddressInput: ->
    AppDispatcher.dispatch({actionType: 'GEOLOC_TOGGLE_ADDRESS_INPUT'})

  selectAddress: (id) ->
    AppDispatcher.dispatch({
      actionType: 'GEOLOC_ADDRESS_SELECTED',
      id: id
    })

}

module.exports = GeolocActions
