AppDispatcher = require '../dispatcher/AppDispatcher'
EventEmitter = require('events').EventEmitter
GeolocProvider =require('../providers/GeolocProvider')
assign = require 'object-assign'
_ = require 'underscore'

CHANGE_EVENT = 'change'

_address = null
_location = null
_processing = []
_predictions = []
_useGeolocation = true
_geolocation = { address: null, location: null }

removeProcessor = (processor) ->
  _processing.splice(_processing.indexOf(processor), 1)

parsePredicitons = (predictions) ->
  return unless predictions
  _predictions = predictions.map (item) ->
    {
      id: item.id, 
      value: item.formatted_address,
      location: { latitude: item.geometry.location.lat(), longitude: item.geometry.location.lng()}
    }

GeolocStore = assign({}, EventEmitter.prototype, {
  isUsingGeolocation: ->
    _useGeolocation

  getAddress: ->
    _address

  getLocation: ->
    _location

  getPredictions: ->
    _predictions

  isProcessing: ->
    _processing.length > 0

  isAddressLocked: ->
    return true if _location != null
    false

  emitChange: ->
    this.emit(CHANGE_EVENT)

  addChangeListener: (callback) ->
    this.on(CHANGE_EVENT, callback)

  removeChangeListener: (callback) ->
    this.removeListener(CHANGE_EVENT, callback)
})

AppDispatcher.register (action) ->
  #console.log 'Receiving ' + action.actionType
  switch action.actionType
    when 'GEOLOC_UPDATE_LOCATION'
      _processing.push('GEOLOC_UPDATE_LOCATION')
      
    when 'GEOLOC_UPDATE_LOCATION_SUCCESS'
      _location = action.location.coords
      _geolocation.location = _location
      removeProcessor('GEOLOC_UPDATE_LOCATION')

    when 'GEOLOC_UPDATE_LOCATION_ERROR'
      removeProcessor('GEOLOC_UPDATE_LOCATION')

    when 'GEOLOC_FIND_ADDRESS'
      _processing.push('GEOLOC_FIND_ADDRESS')

    when 'GEOLOC_FIND_ADDRESS_SUCCESS'
      removeProcessor('GEOLOC_FIND_ADDRESS')
      _address = action.address
      _geolocation.address = _address

    when 'GEOLOC_FIND_ADDRESS_ERROR'
      removeProcessor('GEOLOC_FIND_ADDRESS')

    when 'GEOLOC_UPDATE_ADDRESS'
      _address = action.address

    when 'GEOLOC_QUERY_ADDRESS_SUCCESS'
      parsePredicitons(action.predictions)

    when 'GEOLOC_QUERY_ADDRESS_EMPTY'
      _predictions = []

    when 'GEOLOC_TOGGLE_ADDRESS_INPUT'
      _useGeolocation = !_useGeolocation
      if _useGeolocation 
        _address = _geolocation.address
        _location = _geolocation.location
      else
        _address = ''
        _location = null

    when 'GEOLOC_ADDRESS_SELECTED'
      prediction = _.find _predictions, (elem) -> elem.id == action.id
      if prediction
        _address = prediction.value
        _location = prediction.location

    else
      return true

  GeolocStore.emitChange()
  return true
      
      
module.exports = GeolocStore
