module ApplicationHelper

  def errors_for(model, attribute)
    if model.errors[attribute].present?
      content_tag :span, :class => 'help-block' do
        model.errors[attribute].join(", ")
      end
    end
  end
end
