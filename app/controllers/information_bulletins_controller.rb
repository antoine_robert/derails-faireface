class InformationBulletinsController < ApplicationController
  before_action :require_admin, only: [:destroy,:edit,:update,:create]

  @@per_page = 9

  def index
    @information_bulletins = InformationBulletin.actives
    @information_bulletins = @information_bulletins.search params[:desc_or_title] if params[:desc_or_title]
    @information_bulletins =  @information_bulletins.paginate(:page => params[:page], :per_page => @@per_page)
  end

  def show
    @information_bulletin = InformationBulletin.find params[:id]
  end

  def new
    @information_bulletin = InformationBulletin.new
  end

  def create
    @information_bulletin = InformationBulletin.new
    @information_bulletin.user_id = current_user.id if current_user

    @information_bulletin.assign_attributes(post_params)

    if @information_bulletin.save
      redirect_to :information_bulletins, notice: I18n.t('information_bulletin.created_flash')
    else
      render :new
    end
  end

  def edit
    @information_bulletin = InformationBulletin.find params[:id]
  end

  def update
    @information_bulletin = InformationBulletin.find params[:id]
    if @information_bulletin.update_attributes post_params
      redirect_to :information_bulletins, notice: I18n.t('information_bulletin.created_flash')
    else
      render :action => :edit
    end
  end

  def destroy
    @information_bulletin = InformationBulletin.find params[:id]

    if @information_bulletin.soft_delete
      redirect_to :information_bulletins, notice: I18n.t('information_bulletin.deleted_flash')
    end
  end

  private

    def require_admin
      if !current_user || !current_user.is_admin?
        render :status => :forbidden, :text => t("actions.access_denied")
      end
    end

    def post_params
      permitted_parameters = [:title, :body,:image]
      params.require(:information_bulletin).permit(*permitted_parameters)
    end

end
