class SinistersController < ApplicationController
  before_action :require_admin, only: [:destroy,:edit,:update]

  @@per_page = 9

  def index
    @sinisters = Sinister.actives.order('sinister_date DESC')
    @sinisters = @sinisters.search params[:desc_or_title] if params[:desc_or_title]
    @sinisters = @sinisters.approved unless current_user && current_user.is_admin?
    @sinisters = @sinisters.paginate(:page => params[:page], :per_page => @@per_page)
  end

  def show
    @sinister = Sinister.find params[:id]
  end

  def new
    @sinister = Sinister.new
  end

  def create
    @sinister = Sinister.new
    @sinister.user_id = current_user.id if current_user

    @sinister.assign_attributes(post_params)

    if @sinister.save
      redirect_to :sinisters, notice: I18n.t('sinister.created_flash')
    else
      render :new
    end
  end

  def edit
    @sinister = Sinister.find params[:id]
  end

  def update
    @sinister = Sinister.find params[:id]
    if @sinister.update_attributes post_params
      redirect_to :sinisters, notice: I18n.t('sinister.created_flash')
    else
      render :action => :edit
    end
  end

  def destroy
    @sinister = Sinister.find params[:id]

    if @sinister.soft_delete
      redirect_to sinisters_path, notice: I18n.t('sinister.deleted_flash')
    end
  end

  def approve
    @sinister = Sinister.find params[:sinister_id]

    if @sinister.approve
      redirect_to :back, notice: I18n.t('sinister.approved_flash')
    end
  end

  def unapprove
    @sinister = Sinister.find params[:sinister_id]

    if @sinister.unapprove
      redirect_to :back, notice: I18n.t('sinister.unapproved_flash')
    end
  end

  private
    def post_params
      permitted_parameters = [:title, :description, :sinister_date, :longitude, :latitude, :category_id, :image]
      params.require(:sinister).permit(*permitted_parameters)
    end

end
