class HomeController < ApplicationController
  def index
    @sinisters = Sinister.actives.approved.order(created_at: :desc).limit(3)
  end
end
