class StaticpagesController < ApplicationController
  skip_before_filter :verify_authenticity_token, :only => [:generate_pdf]
  def emergencyplan
  end

  def generate_pdf
    @members= params[:m]
    @res= params[:r]

    render pdf: 'generate_pdf'
  end

  def download_pdf
    send_file(Rails.root + "app/assets/files/PlanFamilialdUrgence_VersionNumerique.pdf",
      filename: "PlanFamilialdUrgence_VersionNumerique.pdf",
      type: "application/pdf")
  end
end