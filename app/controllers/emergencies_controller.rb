class EmergenciesController < ApplicationController
  before_action :require_admin, except: :index

  def index
    @emergencies = Emergency.all
  end

  def new
    @emergencies = Emergency.new
  end

  def create
    @emergencies = Emergency.new

    @emergencies.assign_attributes(post_params)

    if @emergencies.save
      redirect_to :emergencies, notice: I18n.t('emergencies.created_flash')
    else
      render :new
    end
  end

  def edit
    @emergencies = Emergency.find params[:id]
  end

  def update
    @emergencies = Emergency.find params[:id]
    if @emergencies.update_attributes post_params
      redirect_to :emergencies, notice: I18n.t('emergencies.created_flash')
    else
      render :action => :edit
    end
  end

  def destroy
    @emergencies = Emergency.find params[:id]

    if @emergencies.destroy
      redirect_to :back, notice: I18n.t('emergencies.deleted_flash')
    end
  end

  private

    def require_admin
      if !current_user || !current_user.is_admin?
        render :status => :forbidden, :text => t("actions.access_denied")
      end
    end

    def post_params
      permitted_parameters = [:organization, :phone,:web_site]
      params.require(:emergency).permit(*permitted_parameters)
    end

end
