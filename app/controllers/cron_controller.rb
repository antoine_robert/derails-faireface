class CronController < ApplicationController
  def notifications
    alerts = Alert.where(published_at: nil).where('publication_date <= :now', {now: Time.now})
    mailUsers = User.actives.where is_subscribe_mail_alert: true
    phoneUsers = User.actives.where(is_subscribe_mail_alert: true).where.not(phone_number: nil)

    #These should not be hardcoed but put in .env
    twilio_sid = 'AC3c7b711d5a82bdee321ff936d4842689'
    twilio_token = '202d288da5d410de27d64a4395d713bc'
    client = Twilio::REST::Client.new twilio_sid, twilio_token


    alerts.each do |alert|
      mailUsers.each do |user|
        AlertMailer.notification_email(user, alert).deliver!
      end

      phoneBody = alert.title + ' ' + alert.message + ' ' + sinister_url(alert.sinister)
      phoneUsers.each do |user|
        client.account.messages.create({
          from: '+18198014203',
          to: user.phone_number,
          body: phoneBody
        })
      end

      alert.published_at = Time.now
      alert.save
    end

    render text: 'ok'
  end
end
