class Users::ProfileController < ApplicationController

  def edit
    @user = current_user
  end

  def update
    @user = current_user
    @user.assign_attributes(profile_param)

    if @user.save
      redirect_to profile_path, notice: I18n.t('profile.saved')
    else
      render :edit
    end
  end

  def destroy
    @user = current_user
    @user.soft_delete
    sign_out
    redirect_to root_path, notice: I18n.t('profile.destroyed')
  end

  private

    def profile_param
      permitted_parameters = [:name, :phone_number, :is_subscribe_mail_alert, :is_subscribe_phone_alert]
      params.required(:user).permit(*permitted_parameters)
    end
end
