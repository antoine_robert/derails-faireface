class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def google_oauth2
    @user = User.find_for_google_oauth(request.env["omniauth.auth"])
    
    continue_sign_in('Google')
  end

  def facebook
    @user = User.find_for_facebook_oauth(request.env["omniauth.auth"])
    continue_sign_in('Facebook')
  end

  private
    def continue_sign_in(provider)
      if @user.persisted?
        sign_in_and_redirect @user, event: :authentication
        set_flash_message(:notice, :success, kind: provider) if is_navigational_format?
      else
        redirect_to new_user_registration_url, notice: I18n.t('devise.failure.oauth_fail')
      end
    end
end
