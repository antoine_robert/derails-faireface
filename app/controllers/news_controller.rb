class NewsController < ApplicationController
  before_action :require_admin, except: [:show,:index]

  @@per_page = 9

  def index
    @news = New.actives.order('created_at ASC').paginate(:page => params[:page], :per_page => @@per_page)
  end

  def new
    @new = New.new
  end

  def show
    @new = New.find params[:id]
  end

  def create
    @new = New.new
    @new.user_id = current_user.id

    @new.assign_attributes(post_params)

    if @new.save
      session[:active_new_id] = @new.id
      redirect_to news_index_path(:page => find_new_page(@new.id)), flash: { success: I18n.t('new.created_flash') }
    else
      render :new
    end
  end

  def edit
    @new = New.find params[:id]
  end

  def update
    @new = New.find params[:id]
    if @new.update_attributes post_params
      session[:active_new_id] = @new.id
      redirect_to news_index_path(:page => find_new_page(@new.id)), flash: { success: I18n.t('new.updated_flash') }
    else
      render :action => :edit
    end
  end

  def destroy
    @new = New.find params[:id]

    if @new.soft_delete
      if New.actives.order('created_at ASC').paginate(:page => params[:back_page], :per_page => 5).blank?
        page = params[:back_page].to_i - 1
      else
        page = params[:back_page]
      end
      redirect_to news_index_path(:page => page), flash: { success: I18n.t('new.deleted_flash') }
    end
  end

  private

    def require_admin
      if !current_user || !current_user.is_admin?
        render :status => :forbidden, :text => t("actions.access_denied")
      end
    end

    def post_params
      permitted_parameters = [:title, :description]
      params.require(:new).permit(*permitted_parameters)
    end

    def find_new_page(new_id)
      index = New.actives.order('created_at ASC').index(New.find(new_id)).to_i + 1
      (index.to_f / @@per_page.to_f).ceil
    end
end
