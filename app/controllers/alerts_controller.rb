class AlertsController < ApplicationController
  before_action :require_admin, only: [:new, :create, :edit, :update]
  before_action :find_sinister

  def new
    @alert = Alert.new sinister: @sinister
  end

  def create
    @alert = Alert.new sinister: @sinister
    @alert.assign_attributes post_params

    if @alert.save
      redirect_to sinister_path(@sinister), notice: I18n.t('alert.created_flash')
    else
      render :new
    end
  end

  private
    def find_sinister
      @sinister = Sinister.find params[:sinister_id]
      raise ActionController::RoutingError.new unless @sinister
    end

    def post_params
      permitted_parameters = [:title,:publication_date,:message]
      params.require(:alert).permit(*permitted_parameters) 
    end
end
