class AlertMailer < ActionMailer::Base
  default from: "yanickouellet555@gmail.com"

  def notification_email(user, alert)
    @user = user
    @alert = alert
    mail(to: @user.email, subject: alert.title)
  end
end
